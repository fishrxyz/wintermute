#!/bin/bash

MOUNT_POINT=/mnt/wintermute-data
DEVICE_NAME=/dev/xvdf
FS_TYPE=xfs

# update and install xfs progs
sudo apt update && sudo apt install -y xfsprogs

# create the file system
sudo mkfs -t xfs $DEVICE_NAME

# create the mount point
sudo mkdir $MOUNT_POINT

# mount the volume to the mount point
sudo mount $DEVICE_NAME $MOUNT_POINT

# extract the UUID of the device
DEVICE_UUID=$(sudo blkid | grep $DEVICE_NAME | cut -d " " -f 2)

# backup fstab
cp /etc/fstab /etc/fstab.orig

# persist the mount across reboot
# explanation here: https://www.redhat.com/sysadmin/etc-fstab
echo "$DEVICE_UUID  $MOUNT_POINT  $FS_TYPE  defaults,nofail  0  2" >> /etc/fstab

