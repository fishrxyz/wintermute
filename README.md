# Wintermute

Migrate your development environment to AWS.

Wintermute is a recipe powered by terraform and ansible that will create and setup
an EC2 instance on AWS for you that you can provision to your liking.

### What it does

The terraform script will create the following resources for you:

- A KeyPair
- A security group that allows SSH/HTTP(S) connections
- An instance profile
- An EC2 Instance
- An EBS volume
- An Elastic IP (optional but recommended)

_NOTE: depending on the instance type and disk size you chose, you may incur additional charges_

### Instructions

1. Edit the `variables.tf` file and update the values (region, disk size etc)
2. Open the skeleton ansible playbook file located at `ansible/playbook.yml` and add your provisioning tasks
3. Run `make` and watch the magic happen

### A few things to note

- If you decided to change the default keypair name `wintermute_keypair_name`, you will need to edit the makefile to reflect those changes.
