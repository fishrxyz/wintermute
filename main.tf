terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.5.0"
    }
    tls = {
      source = "hashicorp/tls"
      version = "4.0.4"
    }

    local = {
      source = "hashicorp/local"
      version = "2.4.0"
    }
  }
}

provider "aws" {
  region = var.wintermute_region
}

# Create a KeyPair
resource "tls_private_key" "wintermute_private_key" {
  algorithm = "RSA"
  rsa_bits = 4096
}

resource "aws_key_pair" "wintermute_key_pair" {
  key_name = var.wintermute_keypair_name
  public_key = tls_private_key.wintermute_private_key.public_key_openssh
}

resource "local_file" "wintermute_private_key_file" {
  content = tls_private_key.wintermute_private_key.private_key_pem
  filename = "${var.wintermute_keypair_name}.pem"
  file_permission = 400
}

resource "local_file" "wintermute_public_key_file" {
  content = tls_private_key.wintermute_private_key.public_key_openssh
  filename = abspath("./ansible/files/${var.wintermute_keypair_name}.pub")
  file_permission = 644
}

# Create a Security Group
resource "aws_security_group" "wintermute_main_sg" {

  name = "wintermute_name_sg"
  description = "Allow SSH and HTTP(s)"

  ingress {
    description      = "HTTPS rule"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "HTTP rule"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "SSH rule"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow SSH/HTTP(S)"
  }
  
}

resource "aws_iam_role" "wintermute_power_user_role" {
  name = "wintermute_power_user_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
  managed_policy_arns = var.wintermute_instance_role_policies
  tags = { 
    Name = "Wintermute PowerUserAccess Role"
  }
  
}

resource "aws_iam_instance_profile" "wintermute_instance_profile" {
  name = "wintermute_power_user_profile"
  role = aws_iam_role.wintermute_power_user_role.name
}

# Create the instance
resource "aws_instance" "wintermute_instance" {
  ami = var.wintermute_ami
  instance_type = var.wintermute_instance_type
  availability_zone = var.wintermute_default_az
  key_name = aws_key_pair.wintermute_key_pair.key_name
  iam_instance_profile = aws_iam_instance_profile.wintermute_instance_profile.name
  security_groups = [
    aws_security_group.wintermute_main_sg.name
  ]

  ebs_block_device {
    delete_on_termination = true
    device_name = "/dev/sdf"
    encrypted = true
    volume_type = "gp3"
    volume_size = var.wintermute_disk_size

    tags = { 
      Name = "Wintermute Data"
    }
  }

  user_data = file("./scripts/user_data.sh")

  tags = {
    Name = "Wintermute"
  }

}

// resource "aws_eip" "wintermute_elastic_ip" {
//  instance = aws_instance.wintermute_instance.id
//  domain = "vpc"
//}
//
//# Attach it to your instance
//resource "aws_eip_association" "wintermute_elastic_ip_association" {
//  instance_id = aws_instance.wintermute_instance.id
//  allocation_id = aws_eip.wintermute_elastic_ip.id
//}
