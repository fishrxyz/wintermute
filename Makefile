provision: deploy create_inventory
	ansible-playbook -i ansible/inventory.yml ansible/playbook.yml --private-key wintermute.pem --extra-vars "gitlab_token=glpat-_YsqCPy-855dV8z1uCte"

deploy:
	terraform apply -auto-approve

create_inventory:
	terraform output -raw -no-color public_ip > ./ansible/inventory
